'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * Article Schema
 */
var ArticleSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  title: {
    type: String,
    required: true,
    trim: true
  },
  author: {
	type: String,
	required: true,
	trim: true
},
  content: {
    type: String,
    required: true,
    trim: true
  },
  price: {
    type: String,
    required: true,
    trim: true,
	default: 'Best Offer'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  //Hyperlink to book image
  image: {
	type: String,
    required: true,
	trim: true,
	default: '/packages/system/public/assets/img/logo.png' //Default to the website logo if a thumbnail is unavailable
  },
  comments: [String]
});

/**
 * Validations
 */
ArticleSchema.path('title').validate(function(title) {
  return !!title;
}, 'Title cannot be blank');

ArticleSchema.path('content').validate(function(content) {
  return !!content;
}, 'Description cannot be blank');

ArticleSchema.path('price').validate(function(price) {
  return !!price;
}, 'You must specify a price');

/**
 * Statics
 */
ArticleSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('user', 'name username').exec(cb);
};

mongoose.model('Article', ArticleSchema);
